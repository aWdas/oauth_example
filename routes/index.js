module.exports = function (passport) {
    var express = require('express');
    var router = express.Router();

    router.get('/', function(req, res, next) {
        res.render('index', { title: 'Express' });
    });

    router.get('/auth/facebook/',passport.authenticate('facebook'));

    router.get('/auth/facebook/callback', passport.authenticate('facebook', { successRedirect: '/success', failureRedirect: '/' }));

    router.get('/success',function(req,res) {
        console.log(req.user);
        res.render('success', {user: req.user});
    });

    return router;
};
